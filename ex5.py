nmlst = input("Enter two numbers: ").split(",")
num1, num2 = int(nmlst[0]), int(nmlst[1])

def num_checker(num1, num2):
    return (num1 + num2) == 20 or (num1 or num2) == 20

print(num_checker(num1, num2))