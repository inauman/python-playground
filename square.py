#!/usr/bin/python3

square_size = (4, 4)
rows = square_size[0]
cols = square_size[1]

for i in range(rows):
    row_bucket = []
    for j in range(cols):
        col_bucket = "*"
        if not (i ==0 or i == rows - 1):
            if not (j ==0 or j == cols-1):
                col_bucket = " "
        row_bucket.append(col_bucket)    
    print(' '.join(row_bucket))
