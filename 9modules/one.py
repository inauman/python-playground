#one.py

def func():
    print("Hello From One.Func()")

print("Level 0 in one.py")

if __name__ == '__main__':
    print('one.py is being run directly')
else:
    print('one.py imported')
