#!/usr/env/python python3

def splicer(mystr):
    if len(mystr) % 2 == 0:
        return 'EVEN'
    else:
        return mystr[0]


names = ['Andy', 'Eve', 'Sally']

spliced_names = list(map(splicer, names))

print(spliced_names)