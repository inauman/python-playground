#!/usr/bin/env python3

def summer_59(*arr):
    #create the list from *arr which is a tuple
    lst = list(arr)

    #to check multiple elements in an array, use subset/set
    if {6, 9} <= set(arr):

        #now check if 6 is before 9
        if lst.index(6) < lst.index(9):

            # delete the slice from 6 to 9
            del lst[lst.index(6):lst.index(9)+1] # +1 is added as the last number index is not included
    
    #simply return the sum of the items in the list
    return sum(lst)
        
print(summer_59(1,3,5))
print(summer_59(4,5,6,7,8,9,3))
print(summer_59(2,1,6,11))