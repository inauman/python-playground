st = "Print only the words that Start with s in this sentence"

mylist = st.split()

for word in mylist:
    if word.lower().startswith("s"):
        print(word)

