import re

txt = "The rain in Spain falls mainly in the plain"

'''
x = re.search("^The.*Spain$", txt)

if x:
    print("Yay!")
else:
    print("Nay!")

y = re.findall("[a-m]", txt)
print(y)

z = re.findall("aix*", txt)
print(z)

p = re.findall("al{2}", txt)
print(p)

q = re.findall("falls|plain", txt)
print(q)

txt = "8 times before 11:45 AM"

x = re.findall("[0-5][0-9]", txt)

print(x)

'''
print(txt)

#split at each whitespace character 
x = re.split("\s", txt)
print(x)

#print all spaces with 9
x = re.sub("\s", "9", txt)
print(x)
