#!/usr/env/python python3
game_on = True
game_list = [0, 1, 2]

def display_game(game_list):
    print('Here is the current list')
    print(game_list)

def position_choice():
    choice = 'WRONG'
    range_choice = False
    while choice.isdigit() == False or range_choice == False:
        choice = input('Please enter your choice (0, 1, 2): ')

        if choice.isdigit() == False:
            print('Please enter a valid digit [0, 1, 2]')
            continue

        if choice not in ['0', '1', '2']:
            print('Please enter a valid number [0, 1, 2]')
            continue
        else:
            range_choice = True
    return int(choice)

def replacement_choice(game_list, position):
    user_placement = input("Type a string to place at position: ")

    game_list[position] = user_placement

    return game_list

def gameon_choice():
    choice = 'WRONG'

    while choice.upper() not in ['Y', 'N']:
        choice = input('Keep playing? (Y or N): ')

        if choice.upper() not in ['Y', 'N']:
            print("Sorry! I don't understand, please enter Y or N")


    if choice.upper() == "Y":
        return True
    else:
        return False

while game_on:
    display_game(game_list)
    position = position_choice()
    game_list = replacement_choice(game_list, position)
    display_game(game_list)
    game_on = gameon_choice()