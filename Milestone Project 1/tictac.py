#!/usr/env/python python3

# 1. Setup the 3x3 board
board = [' ',' ',' ',' ',' ',' ',' ',' ',' ']
players = []
# 2. Display the board
def display_board(board):
    print(board[0] + "  | " + board[1] + "  | " + board[2])
    print("__" + "  " + " __" + "  " + "  __")
    print(board[3] + "  | " + board[4] + "  | " + board[5])
    print("__" + "  " + " __" + "  " + " __")
    print(board[6] + "  | " + board[7] + "  | " + board[8])

# 2. Setup players
def setup_players():

    # Player 1 information
    player1 = input("Player 1 - Enter your name: ")
    
    choice = "WRONG"

    # Ask first player choice
    while choice.upper() not in ['Y', 'N']:
        choice = input("Player 1 - Do you want to go first? (Y, N): ")

        if choice not in ['Y', 'N']:
            print("Please enter valid choice (Y or N")
        
        if choice == 'Y':
            p1 = 'X'
            p2 = 'O'
        else:
            p1 = 'O'
            p2 = 'X'


    print(f'Ok! {player1} you got {p1} to play the game!')
    
    # Player 2 information
    player2 = input("Player 2 - Enter your name: ")

    print(f'{player2} you got {p2} to play the game!')

    # Setup players
    if p1 == 'X':
        players = [{player1:[p1,' ']}, {player2:[p2,' ']}]
    else:
        players = [{player2:[p2,' ']}, {player1:[p1,' ']}]

    return players
  
# 3. Keep the game on?
def gameon_choice(flag=True):
    gameon = flag

    while gameon == True:
        for player in players:

            # Get the player name from the map in the list
            coordinates = take_turn(player)

            # Refresh the board after the move
            refresh_board(coordinates[0], coordinates[1])
    else:
        # Exit the game
        winner(board)
        print("Thank you for playing tictac. Have a nice day!")
        exit()

def take_turn(player):
    choice = 'WRONG'
    range_choice = False
    while choice.isdigit() == False or range_choice == False:
        player_name = list(player.keys())[0]
        choice = input(f"{player_name}! Please enter your choice (1 - 9): ")
        #print(f'{player} made a choice {choice}')
        if choice.isdigit() == False:
            print(f"{player_name}! Please enter a valid number")
            continue
        
        # stop the game if choice is 0
        if choice == '0':
            gameon_choice(False)
        # Valid range 
            #1 Must be between 1 and 9
            #2 Can't overwrite existing choices. Second check is to implement immutable behavior in the list
        if int(choice)-1 not in range(0,9) or board[int(choice)-1] != ' ':
            print(f"{player_name}! Enter a valid number (1 - 9). No overwirting allowed.")
        else:
            range_choice = True

    return [int(choice)-1, player.get(player_name)[0]]

# 4. Replace board cell with the choice
def refresh_board(position, symbol):
    board[position] = symbol
    display_board(board)

def winner(board):
    i = 0
    # Check the board for horizontal row strike
    for i in range(0,9,3):
        if board[i] != ' ' and (board[i] == board[i+1] == board[i+2]):
            print(f'{board[i]} hit a strike')

    # Check the board for vertical column strike
    for j in range(0,3,1):
        if board[j] != ' ' and (board[j] == board[j+3] == board[j+6]):
            print(f'{board[j]} hit a strike')

# TODOs
# track the winner of 3 strikes of X or O 

# Setup the game
display_board(board)
players = setup_players()
gameon_choice()
