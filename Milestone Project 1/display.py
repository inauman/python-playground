#!/user/env/python python3

row1 = [' ',' ', ' ']
row2 = [' ',' ', ' ']
row3 = [' ',' ', ' ']

def user_choice():
    choice = 'WRONG'
    within_range = False
    while choice.isdigit() == False or within_range == False:
        choice = input("Please enter a number (1 - 9): ")

        # Check 1: DIGIT
        if choice.isdigit() == False:
            print("Sorry that is not a digit!")
            continue
        
        # Check 2: WITHIN_RANGE
        if int(choice) not in range(1,10):
            print("Sorry the number is not in range (1 - 9)!")
        else:
            within_range = True
        
    return int(choice)

print(user_choice())



# Simple tic tac toe display
def display(row1, row2, row3):
    print(row1)
    print(row2)
    print(row3)

row2[1] = 'X'
#display(row1, row2, row3)


