#!/usr/env/python python3

x = 25

def printer(x):
    x = 50
    return x

# Avoid using global as a good practice
def printer():
    global x
    x = 50
    return x

print(x)
print(printer())
x = printer()
print(x)