fruits = ["apple","orange","banana", "mango", "watermelon"]
'''
for x in fruits:
    print(x)
'''

'''
for x in "banana": #string is also a sequence!
    print(x)
'''

'''
for x in fruits: #break #continue
    if x == "banana":
        break #continue
    print(x)
'''
'''
#range
for x in range(2, 30, 3):
    print(x)
'''
'''
#else
for x in range(6):
    print(x)
else:
    print("finally done!")
'''

print([i for i in range(0, 20)])