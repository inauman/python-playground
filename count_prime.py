#!/usr/env/python python3

def count_prime(num):

    # Check for 0 or 1 input
    if num < 2:
        return 0

    # 2 or greater

    # Store our prime numbers
    primes = [2]

    # Counter going upto the input num
    x = 3

    # x is going through every number up to input num
    while x <= num:

        # Check if x is prime
        #for y in range(3,x,2):
        for y in primes:
            print(f'{x} % {y}')
            if x % y == 0:
                x += 2
                break
        else:
            primes.append(x)
            x += 2
    
    print(primes)
    print(f'\nThere are total {len(primes)} prime numbers\n')

count_prime(30)