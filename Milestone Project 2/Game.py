from Player import Player
from Deck import Deck

class Game():

    # Game setup
    def __init__(self):

        self.game_on = True
        self.player_one = Player("One")
        self.player_two = Player("Two")

        self.new_deck = Deck()
        self.new_deck.shuffle()
    
        for i in range(26):
            self.player_one.add_cards(self.new_deck.deal_one())
            self.player_two.add_cards(self.new_deck.deal_one())
    
    def play_game(self):
        round_num = 0
        # while game_on loop
        while self.game_on:
            round_num += 1
            print(f'Round # {round_num}')

            if len(self.player_one.all_cards) == 0:
                print('Player One, out of cards! Player Two Wins!')
                self.game_on = False
                break

            if len(self.player_two.all_cards) == 0:
                print('Player Two, out of cards! Player One Wins!')
                self.game_on = False
                break
            
            # Start a new round
            player_one_cards_in_play = []
            player_one_cards_in_play.append(self.player_one.remove())

            player_two_cards_in_play = []
            player_two_cards_in_play.append(self.player_two.remove())

            # Check for at War condition i.e. when the two cards have same rank
            # Start as if at_war is True
            at_war = True
            while at_war:

                if player_one_cards_in_play[-1].value > player_two_cards_in_play[-1].value:
                    # Player One collect all the cards
                    self.player_one.add_cards(player_one_cards_in_play)
                    self.player_one.add_cards(player_two_cards_in_play)
                    at_war = False

                elif player_one_cards_in_play[-1].value < player_two_cards_in_play[-1].value:
                    # Player Two collect all the cards
                    self.player_two.add_cards(player_one_cards_in_play)
                    self.player_two.add_cards(player_two_cards_in_play)
                    at_war = False
                else:
                    print('WARRRRRR!!!!!!')
                    if len(self.player_one.all_cards) < 5:
                        print("Player One unable to declare war")
                        print("Player Two Wins!!!!")
                        at_war = False
                        self.game_on = False
                        
                    elif len(self.player_two.all_cards) < 5:
                        print("Player Two unable to declare war")
                        print("Player One Wins!!!!")
                        at_war = False
                        self.game_on = False
                
                    else:
                        for i in range(5):
                            player_one_cards_in_play.append(self.player_one.remove())
                            player_two_cards_in_play.append(self.player_two.remove())

if __name__ == '__main__':
    g = Game()
    g.play_game()