import random

from Card import *

class Deck():

    def __init__(self):
        
        self.all_cards = []

        for suit in suits:
            for rank in ranks:
                # Create a card object
                created_card = Card(suit,rank)

                # Add the card to the deck
                self.all_cards.append(created_card)
    
    def shuffle(self):
        # Shuffule works in place
        random.shuffle(self.all_cards)

    def deal_one(self):
        return self.all_cards.pop()

'''
new_deck = Deck()
new_deck.shuffle()
print(len(new_deck.all_cards))
mycard = new_deck.deal_one()
print(len(new_deck.all_cards))
print(mycard)
'''