#!/usr/bin/env python3

mylist = [x for x in range(1,10,2)]

for item in mylist:
    print(item)

print("End of Script")
