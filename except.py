'''
try:
    print(x)

except NameError:
    print("Variable x is not defined")

except:
    print("an exception occured!")


try:
    print("Hello")
except:
    print("An error occured")
else:
    print("All went well without any error")
finally:
    print("you can do some house cleaning here")

'''

'''
try:
    f = None
    f = open("demofile.txt", "r") #file won't be created as flag is r
    f.write("Hello World!")
except:
    print("Arghhhh! something went wrong")
finally:
    if f != None:
        f.close()
    else:
        print("Nothing to clean")

'''
'''
x = -1

if x < 0:
    raise Exception("Sorry, no numbers below 0")
'''
x = "hello"

if not type(x) is int:
    raise TypeError("Only integers are allowed")