import numpy as np
'''
#create using list
arr1 = np.array([1, 2, 3, 4, 5])
print(arr1)

#create using tuple
arr2 = np.array((1,2,3,4,5))
print(arr2)

#create 0-D array (each element is a 0-D array i.e. scalar)
arr3 = np.array(42)
print(arr3)

#1-D array consist of 0-D arrays
arr4 = np.array((1,2,3,4,5))
print(arr4)

#2-D
arr5 = np.array([[1,2,3],[4,5,6]])
print(arr5)

#check dimension of array
print(arr5.ndim)
'''

arr6 = np.array([1,2,3,4])
print(arr6[3])
print(arr6.ndim)

arr7 = np.array([[1,2,3],[4,5,6],[7,8,9]])
print(arr7[2,1])
