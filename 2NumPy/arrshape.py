import numpy as np

arr = np.array([1,2,3,4,5,6,7,8,9,10,11,12])

#4x3
newarr = arr.reshape(4,3)

#1D --> 3D
newarr2 = arr.reshape(2,3,2)

#print(newarr2)

#print(newarr2.base)

arr = np.array([[1, 2, 3], [4, 5, 6]])

for x in arr:
  print(x)

for x in arr:
  for y in x:
    print(y)