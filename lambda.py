#!/usr/env/python python3
# Setup a list
mylist = [1,2,3,4,5]

# Apply a square lambda to the above list
squared_list = list(map(lambda num: num ** 2, mylist))

print(squared_list)

# Apply a filter
filtered_square_list = list(filter(lambda num: num % 2 == 0, squared_list))

print(filtered_square_list)