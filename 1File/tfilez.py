f = open("demofile2.txt", "a")
f.write("This is a file writing (append) example\n")
f.close()

#open and read the file
f = open("demofile2.txt")
print(f.read())

#will throw the error if file already exist!
#f = open("demofile.txt", "x")