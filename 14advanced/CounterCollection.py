# Counter - a special class of Dict
from collections import Counter

mylist = [1,2,2,3,4,4,5,1,1,2,3,4,4,2,1,1,2,2]

print(Counter(mylist))

print(Counter("Mohammad Nauman"))

st = "I have the best family in the world"
st_list = st.lower().split(" ")
print(Counter(st_list))

letters = "aaaaaabbbbbcccddddddd"
c = Counter(letters)
print(c.most_common(1))
print(list(c))

