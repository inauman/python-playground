import re

text = "The agent's phone number is 408-555-1234. Call phone soon!"

#phone = re.search(r'\d\d\d-\d\d\d-\d\d\d\d', text)

# use quantifiers
phone = re.search(r'\d{3}-\d{3}-\d{4}', text)
print(phone.group())

# use compile to group the patterns
phone_pattern_compiled = re.compile(r'(\d{3})-(\d{3})-(\d{4})')

phone = re.search(phone_pattern_compiled, text)

print(phone.group(1))