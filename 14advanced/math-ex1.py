import random


# same "seed" produces the same series of random numbers




mylist = list(range(0, 21))

print(mylist)

# In place shuffle
random.shuffle(mylist)
print(mylist)

# One random choice
print(random.choice(mylist))

# Sample with replacement
print(random.choices(population=mylist,k=10))

# Sample without replacement
print(random.sample(population=mylist,k=10))

