import zipfile
import os
#Zipper
comp_file = zipfile.ZipFile('comp_file.zip', 'w')

# create bunch of file
for i in range(10):
    with open(f'file{i}.txt', 'w+') as f:
        f.write(f"This is file: file{i}.txt")
    
    # add each file to zip
    comp_file.write(f'file{i}.txt', compress_type=zipfile.ZIP_DEFLATED)
    
    # remove the individual file
    os.remove(f'file{i}.txt')
comp_file.close()

# Unzipper
zip_obj = zipfile.ZipFile('comp_file.zip', 'r')
zip_obj.extractall('extracted_content')