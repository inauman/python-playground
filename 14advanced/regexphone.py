import re

def is_phone_number(number):
    pattern = r'\W*(\d{3})\W*(\d{3})\W(\d{4})'
    phone_number_pattern = re.compile(pattern=pattern)
    match = re.findall(phone_number_pattern, number)
    return len(match) != 0

is_phone_number('480.327.9393')