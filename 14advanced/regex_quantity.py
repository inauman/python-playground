import re

pattern = r'Bat(wo)?man'

text1 = 'The adventures of Batman'
text2 = 'The adventures of Batwoman'

mo1 = re.search(pattern, text1)

print(mo1.group())

mo2 = re.search(pattern, text2)

print(mo2.group())