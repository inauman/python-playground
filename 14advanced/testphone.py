import unittest
import regexphone

class TestPhone(unittest.TestCase):

    def test_one_all_dashes(self):
        text = '480-327-0000'
        result = regexphone.is_phone_number(text)
        self.assertEqual(result, True)

    def test_two_all_dots(self):
        text = '480.327.0000'
        result = regexphone.is_phone_number(text)
        self.assertEqual(result, True)
    
    def test_three_all_spaces(self):
        text = '480 327 0000'
        result = regexphone.is_phone_number(text)
        self.assertEqual(result, True)

    def test_four_all_alpha_mixed(self):
        text = '480327000a'
        result = regexphone.is_phone_number(text)
        self.assertEqual(result, False)

    def test_five_all_parenthesis(self):
        text = '(480) 327 0000'
        result = regexphone.is_phone_number(text)
        self.assertEqual(result, True)

    def test_six_all_dot_dash(self):
        text = '480.327-0000'
        result = regexphone.is_phone_number(text)
        self.assertEqual(result, True)
    
    def test_six_all_text_with_embedded_phone(self):
        text = 'This is my phone (480) 327 0000'
        result = regexphone.is_phone_number(text)
        self.assertEqual(result, True)

if __name__ == '__main__':
    unittest.main()
