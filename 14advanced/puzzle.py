import shutil
import os
import re

# Working folder
working_folder_path = os.path.join('/Users/nauman', 'Downloads')

# Read and unzip the archive
file_to_unzip = os.path.join(working_folder_path, 'unzip_me_for_instructions.zip')

# extracted folder path
extracted_folder_path = os.path.join(working_folder_path, 'extracted_content')

# Read the Instructions.txt from the extracted_content folder
instructions_file = os.path.join(extracted_folder_path, 'Instructions.txt')

shutil.unpack_archive(file_to_unzip, working_folder_path, 'zip')

with open(instructions_file, 'r') as f:
    print(f.read())

def search(file, pattern=r'\d{3}-\d{3}-\d{4}'):
    with open(file, 'r') as f:
        text = f.read()
    
    if re.search(pattern, text):
        return re.search(pattern, text)
    else:
        return ''
'''
results = []
for folder, sub_folder, files in os.walk(extracted_folder_path):
    for f in files:
        if f not in ('.DS_Store', 'Instructions.txt'):
            full_path = os.path.join(folder, f)
            results.append(search(full_path))
    print(results)
'''
for folder in os.listdir(extracted_folder_path):
    if folder not in ('.DS_Store', 'Instructions.txt'):
        dir = os.path.join(extracted_folder_path, folder)
        for file_name in os.listdir(dir):
            full_file_path = os.path.join(dir, file_name)
            with open(full_file_path, 'r') as f:
                contents = f.read()
                pattern = r'\d{3}-\d{3}-\d{4}'
                match = re.findall(pattern,contents)
                try:
                    print(f'{file_name} {match[0]}')
                except:
                    pass

