import shutil
import os

dir_to_zip = os.path.join(os.getcwd(), 'extracted_content')

output_filename = "example"

# zip using shutil
shutil.make_archive(output_filename, 'zip', dir_to_zip)

# unzip using shutil
shutil.unpack_archive('example.zip', 'final_unzip', 'zip')