import re

pattern = r'Bat(man|copter|mobile|bat)'

bat_regex = re.compile(pattern)

text = 'Batcopter lost a wheel'

match = bat_regex.search(text)

print(match.group(1))