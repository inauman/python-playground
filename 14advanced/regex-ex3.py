import re

pattern = "cat"

text = "This is a cat!"

# or operator
match1 = re.search(r'cat|dog', text)
print(match1.group())

# wildcard
match2 = re.findall(r'.at', "The cat in the hat sat there")
print(match2)

# ^ for start $ for end
match3 = re.findall(r'\d$', "The number is 2")
print(match3)

# exclusion [^]
match4 = re.findall(r'[^\d]+', "There are 3 numbers 34 inside 5 this sentance")
print(match4)

# clean (exclude) unwanted characters
phrase = "This is a string! But it has punctuation. How can we remove it?"
clean = re.findall(r'[^!.? ]+', phrase)
print(' '.join(clean))

# inclusion
text = 'Only find the hyper-words in this sentence. But you do not know how long-ish they are'
match5 = re.findall(r'[\w]+-[\w]+', text)
print(match5)

# last example
textone = 'Hello, would you like some catfish?'
texttwo = 'Hello, would you like to take a catnap?'
textthree = 'Hello, have you seen this caterpillar?'

match6 = re.search(r'cat(fish|nap|claw)', textthree)
print(match6)


