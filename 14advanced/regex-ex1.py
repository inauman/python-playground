import re

text = "The agent's phone number is 408-555-1234. Call phone soon!"

pattern = 'phone'

match = re.search(pattern, text)
print(match.span())

matches = re.findall(pattern, text)
print(matches)

for match in re.finditer(pattern, text):
    print(match.group())
