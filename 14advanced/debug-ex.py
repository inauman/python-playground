import pdb

x = [1, 2, 3]
y = 2
z = 3

result_one = y + z

# set the trace to see variable values
# q for quit
pdb.set_trace()

result_two = y + x
