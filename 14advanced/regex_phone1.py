import re

# area code optional
pattern = r'(\d{3}-)?\d{3}-\d{4}'

text1 = "My number is 602-284-0000"
text2 = "Your number 284-0000"

mo = re.search(pattern, text1)
print(mo.group())

mo = re.search(pattern, text2)
print(mo.group())