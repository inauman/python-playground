def sum(k):
    if(k > 0): #keep going
        result = k + sum(k-1)
        print(result)
    else: #base or terminating condition
        result =  0
        print(result)
    return result

print("\n\n Recurion Results")
sum(6)