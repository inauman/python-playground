#!/usr/bin/env python3

def has_33(nums):
    for i in range(0, len(nums)-1):
        if nums[i] == nums[i+1] == 3:
            return True
            
    # important: return True and False shouldn't be at the same hierarchy
    return False

print(has_33([1,3,3]))
print(has_33([1,3,1,3]))
print(has_33([3,1,3]))

