#!/usr/bin/env python3

def find_prime(number):
    count = 3
    for m in range(2, number):
        for n in range(2, number):
            if m % n == 0:
                break
            else:
                count += 1
                continue
        else:
            count += 1
            continue

    return count

print(find_prime(100))
                

