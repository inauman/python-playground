class Person:
    def __init__(self, _name, _age):
        self.name = _name
        self.age = _age

    def myfunc(self):
        print("Hello my name is "+ self.name)

p1 = Person("John", 35)
p1.myfunc()
p1.name = "Mayesha"
p1.myfunc()