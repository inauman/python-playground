#!/usr/bin/env python3
import sys

params = tuple(sys.argv)
name, n1,n2,n3 = sys.argv
 
def blackjack(a, b, c):

    if sum([a,b,c]) <= 21:
        return sum([a,b,c])
    elif 11 in [a,b,c] and sum([a,b,c]) <= 31:
        return sum([a,b,c]) - 10
    else:
        return "BUST"
    
print(blackjack(int(n1), int(n2), int(n3)))