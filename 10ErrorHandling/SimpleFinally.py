try:
    f = open('testfile', 'w')
    f.write("This is a test")
except TypeError:
    print("There was a TypeError")
except OSError:
    print('Hey you have an OS Error')
except:
    print("All other errors")
finally:
    print("I always run")
    