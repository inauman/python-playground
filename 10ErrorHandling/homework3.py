from typing import Sequence


def ask():
    while True:
        try:
            n = input("Please enter a number: ")
            sqn = int(n) ** 2
        except:
            print("you entered incorrect data type")
            continue
        else:
            print(f"Success! {sqn}")
            break
    return sqn
ask()