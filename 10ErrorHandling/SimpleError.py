n1 = 10
n2 = input("Please enter n2: ")

def add(n1, n2):
    try:
        my_sum = n1 + int(n2)
    except:
        print("You are trying to add incompatible numbers")
    else:
        print(f'Nice job! {my_sum}')

add(n1, n2)