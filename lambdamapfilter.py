#!/usr/env/python python3

def square(num):
    return num**2

my_nums = [1,2,3,4,5]

#for item in map(square, my_nums):
#    print(item)

squared_list = list(map(square, my_nums))

print(squared_list)