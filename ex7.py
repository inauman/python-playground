#given a sentence, reverse the words

sentence = input('Enter the sentence here: ')

def reverser(sentence):
    wordlst = sentence.split(" ")

    return " ".join(wordlst[::-1])

print(reverser(sentence))