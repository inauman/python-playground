def hello(name="Mayesha"):
    print('The hello function executed!')

    def greet():
        return '\t This is the greet() func inside hello!'

    def welcome():
        return '\t This is the welcome() inside hello'
    
    print("I am going to return a function")

    if name == 'Mayesha':
        return greet
    else:
        return welcome

my_new_func = hello('Mayesha')
print(my_new_func())
