import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017/")

mydb = myclient["mydatabase"]

#create a collection i.e. table
mycollection = mydb["customers"]

for x in mycollection.find({}, {"_id": 0, "name": 1, "address": 1 }):
    print(x)

#exclude address
for x in mycollection.find({}, { "address": 0 }):
    print(x)