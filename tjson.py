import json

x = '{ "name":"John", "age":30, "city":"New York"}'

y = json.loads(x)

print(y["age"])

m = { 
    "name":"John", 
    "age":30, 
    "city":"New York"
}

n = json.dumps(m)

print(n)