#!/usr/bin/env python3

#from futureemployees import actions

class NextCareerMove():
    
    def __init__(self):
        self.open_to_opportunities = True
        self.recruiter_email = input('Please enter recruiter email: ')
        self.candidate_email = input('Please enter candidate email: ')

    def schedule_chat(self):
        print(f'Scheduling chat with {self.candidate_email}')
        pass
    
    def refer_a_friend(self):
        pass

    def send_thank_you(self):
        pass

    def save_candidate_info(self):
        print
        pass

    def job_hunting(self):
        if self.open_to_opportunities:
            # Schedule chat with the candidate
            self.schedule_chat(self)
        else:
            # Ask for strong candidate refferal
            self.refer_a_friend()

            # Send thank you note
            self.send_thank_you()

            # Save information for future reference
            self.save_candidate_info()

ncm = NextCareerMove()