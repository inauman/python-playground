import random

def rand_number(low, high, n):
    for i in range(n):
        yield random.randint(low, high)

for x in rand_number(5, 23, 7):
    print(x)