import requests

site_url = 'https://automatetheboringstuff.com/files/rj.txt'
res = requests.get(site_url)

# If there is an error, raise an exception and exit
try:
    res.raise_for_status()
except:
    print("Sorry! couldn't download the file")

# Else continue. It is like checking for status code 200
with open('RomeoAndJuliet.txt', 'wb') as f:
    for chunk in res.iter_content(100000):
        f.write(chunk)

