import requests
import bs4

results = requests.get("https://en.wikipedia.org/wiki/Deep_Blue_(chess_computer)")

soup = bs4.BeautifulSoup(results.text, 'lxml')

image = soup.select("img")[0]

image_link = requests.get("https:" + image['src'])

f = open('/Users/nauman/Downloads/My_computer_image.jpg', 'wb')
f.write(image_link.content)
f.close()