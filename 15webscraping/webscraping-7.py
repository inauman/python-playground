import bs4, requests

# 1 Open the page
site_url = 'https://www.amazon.com/Automate-Boring-Stuff-Python-2nd/dp/1593279922/'
res = requests.get(site_url)

try:
    res.raise_for_status()
except:
    print("Sorry! Couldn't open the page")

# Make a soup of the page content
soup = bs4.BeautifulSoup(res.text, 'lxml')
price_element = soup.select('#a-autoid-1-announce > span.slot-price > span')

price = price_element[0].text.strip()

print(price)