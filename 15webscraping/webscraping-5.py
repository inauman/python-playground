import webbrowser, os, sys
import pyperclip

#site_url = 'https://automatetheboringstuff.com/'
site_url = 'https://www.google.com'
webbrowser.open(site_url)

# Use third-party module - pyperclip - to copy & paste
text_from_clipboard = pyperclip.paste()

print(text_from_clipboard)