import requests
import bs4

base_url = "https://quotes.toscrape.com"

res = requests.get(base_url)

soup = bs4.BeautifulSoup(res.text, 'lxml')



# Get the unite list of authors (convert list to set) on the first page
authors_scrape = soup.select('.author')

authors = []
for author in authors_scrape:
    authors.append(author.text)

#print(set(authors))

print('\n')

# Get all the quotes from first page
quotes_scrape = soup.select('.quote > span.text')

quotes = []
for quote in quotes_scrape:    
    quotes.append(quote.text)

#print(quotes)

# Get top 10 tags from the first page
top10_scrape = soup.select('.tag-item > a.tag')

top10 = []
for item in top10_scrape:    
    top10.append(item.text)

#print(top10)
authors_list = []

def unique_authors():
    page_num = 1
    while True:
        
        page_url = base_url + '/page/{}/'.format(page_num)

        page_num += 1

        res = requests.get(page_url)
        if 'No quotes found!' not in res.text:
            soup = bs4.BeautifulSoup(res.text, 'lxml')
            authors_scrape = soup.select('.author')
            for author in authors_scrape:
                authors_list.append(author.text)
            continue
        else:
            break
    # Print the sorted author list
    print(sorted(set(authors_list)))
    

unique_authors()

