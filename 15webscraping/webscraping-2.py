import requests
import bs4

result = requests.get("https://www.wikipedia.org/wiki/Grace_Hopper")

soup = bs4.BeautifulSoup(result.text, 'lxml')

toc = soup.select('.toctext')
for item in toc:
    print(item.text)
