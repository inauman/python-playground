#!/user/env/python python3
import string

def ispangram(s, alphabet=string.ascii_lowercase):
    flag = False

    s = s.replace(" ", "").lower()
    for letter in alphabet:
        if not s.__contains__(letter):
            break
    else:
        flag = True

    print(flag)

#True
ispangram("The quick brown fox jumps over the lazy dog")

#False
ispangram("This is a cat")

