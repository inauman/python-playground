numst = input('Enter the two numbers here: ')

numlst = numst.split(',')

str1, str2 = tuple(numlst)

num1, num2 = int(str1), int(str2)

def lesser_of_two_evens(num1, num2):
    if (num1 %2 != 0) or (num2 %2 != 0):
        return max(num1, num2)
    else:
        return min(num1, num2)

print(lesser_of_two_evens(num1, num2))