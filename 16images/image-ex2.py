from PIL import Image

red = Image.open('red_color.jpg')
red.putalpha(128)
#red.show()

blue = Image.open('blue_color.png').convert('RGB')
blue.putalpha(128)

blue.paste(im=red,box=(0,0),mask=red)
blue.save("purple.png")