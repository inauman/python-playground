'''
x = lambda a, b : a * b
print(x(5,6))
'''

def my_function(n):
    return lambda a : a * n

doubler = my_function(2)

print(doubler(11))

tripler = my_function(3)

print(tripler(11))