import csv

# Open the file
data = open("example.csv", encoding='utf-8')

# csv.reader
csv_data = csv.reader(data)

# reformat it into a python object list of lists
data_lines = list(csv_data)

all_emails = []
for line in data_lines[1:5]:
    all_emails.append(line[3])

print(all_emails)

# Write to csv file
file_to_output = open("to_save_file1.csv", 'w', newline='')

csv_writer = csv.writer(file_to_output, delimiter=',')

csv_writer.writerow(['Name','Role','Company'])
csv_writer.writerows([['Nauman','Manager','Schwab'],['Mayesha','CEO','Mayehsa Enterprise']])
file_to_output.close()

# Append to existing file
file_to_output = open("to_save_file1.csv", 'a', newline='')
csv_writer = csv.writer(file_to_output, delimiter=',')
csv_writer.writerows([['Sam','Manager','Home']])
file_to_output.close()
