import csv
import PyPDF2
import re

# Open CSV file
f = open('Exercise_Files/find_the_link.csv', 'r')

# read the CSV data from the file
csv_data = csv.reader(f, delimiter=',')

# Transform the csv data into python list
data_lines = list(csv_data)

# For the puzzle, the URL is embedded diagonally in the list
drive_url_codes = []

for i in range(len(data_lines)):
    drive_url_codes.append(data_lines[i][i])

drive_url = ''.join(drive_url_codes)

# download the pdf file from this URL
print(drive_url)

f = open('Exercise_Files/Find_the_Phone_number.pdf', 'rb')
pdf = PyPDF2.PdfFileReader(f)


# Find a phone which can have any kind of separator except that will be grouped in 3,4,4 digits 
pattern = r'\d{3}\W\d{3}\W\d{4}'
for i in range(pdf.numPages):
    page = pdf.getPage(i)

    pageText = page.extractText()

    matches = re.findall(pattern, pageText)
    if len(matches) != 0:
        print(matches[0])

f.close()




