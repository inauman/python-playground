def myfunc(*args, **kwargs):
    print(f'my fav number is {args} and my fav fruit is {kwargs["fruit"]}')

myfunc(10,20,30,40,50,veggie='carrot',fruit="apple",animal="dog")