class mycalss():
    def __len__(self):
        return 0

myobj = mycalss()
print(bool(myobj)) #if __len__ returns 0, it bool is false, else true

def myFunc():
    return True

if myFunc():
    print("Yes")
else:
    print("No")