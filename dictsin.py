idName = {
    300: "Mohammad Nauman",
    400: "Shabnam Nauman",
    500: "Maaz Nauman",
    600: "Ayesha Nauman"
}


def greeting(_userid):
    return "Hi %s!" % idName.get(_userid, "there")

print(greeting(300))
print(greeting(700))