def myfunc(*args):
    print(type(args))
    print(args)
    return sum(args) * .05

print(myfunc(40, 60, 20, 80))

def my_key_func(**kwargs):
    print(type(kwargs))
    print(kwargs)

my_key_func(a=1,b=2, c = 8)

def func(*args, **kwargs):
    print(f"I would like to eat {args[0]} {kwargs['food']}")

func(10, 20, 30, fruits='orange', food='eggs', animal='dog' )
