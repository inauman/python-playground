age = 36
txt = "My name is John, and I am {}"
print(txt.format(age))

qty = 3
item = 4322
price = 49.99

order = "I want {} pieces of {} for {} dollars"

print(order.format(qty, item, price))

order = "I want to pay {2} dollars for {0} pieces of {1}"

print(order.format(qty, item, price))