class MyNumbers:
    def __iter__(self):

        self.a = 5
        return self
    
    def __next__(self):
        if self.a <= 20:
            x = self.a
            self.a += 1
            return x
        else:
            print("I am done!")
            raise StopIteration

myclass = MyNumbers()
myiterator = iter(myclass)
for x in myiterator:
    print(x)

