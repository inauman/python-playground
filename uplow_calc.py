#!/usr/env/python python3

def up_low(s):
    d = {'up': 0, 'low' : 0}

    for i in s:
        if i.islower():
            d['low'] += 1
        elif i.isupper():
            d['up'] += 1
    print(f"Upper Case: {d['up']} &&& lower case: {d['low']}")

up_low("This is a Test")