from functools import reduce

class MapR():
    simple_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    def square(self, num):
        return num**2

    # Less common, use a function to transform an iterable
    def square_list(self):
        squared_list = list(map(self.square, MapR.simple_list))
        print(squared_list)

    # More common, use lambdas to transform iterables
    def cube_list(self):
        cubed_list = list(map(lambda x: x ** 3, MapR.simple_list))
        print(cubed_list)

    def filter_list(self):
        filtered_list = list(filter(lambda x: x % 2 == 0, MapR.simple_list))
        print(filtered_list)

    def reduce_list(self):
        reduced_list = reduce(lambda x, y: x + y, MapR.simple_list)
        print(reduced_list)

mapr = MapR()
mapr.square_list()
mapr.cube_list()
mapr.filter_list()
mapr.reduce_list()
