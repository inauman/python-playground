class Line():

    def __init__(self, coor1, coor2):
        self.x1, self.y1 = coor1
        self.x2, self.y2 = coor2
    
    def distance(self):
        d = ((self.x2-self.x1)**2 + (self.y2-self.y1)**2)**0.5
        print(f'distance is {d}')

    def slope(self):
        m = (self.y2-self.y1)/(self.x2-self.x1)
        print(f'slope is {m}')

li = Line((3,2),(8,10))
li.distance()
li.slope()