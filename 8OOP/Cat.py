# Note the import syntax for classes
# the first name is for the module i.e. .py class
# second part is the class which we want to import
# A module i.e. .py file can have multiple classes
from Animal import Animal

class Cat(Animal):
    
    def who_am_i(self):
        print("I am a cat!")

    def color(self):
        print("I am pink color!")

my_cat = Cat("Katerina")
my_cat.eat()
my_cat.who_am_i()
my_cat.color()