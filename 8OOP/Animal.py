class Animal():

    def __init__(self, name):
        self.name = name
        print("Animal Created")

    def who_am_i(self):
        print("I am an Animal")

    def eat(self):
        print('I am eating')

    def color(self):
        raise NotImplementedError("Subclass must implement this method")