class Cylinder():
    pi = 3.14

    def __init__(self, height=1, radius=1):
        self.height = height
        self.radius = radius

    
    def volume(self):
        vol = Cylinder.pi * self.height * self.radius**2
        print(vol)
        
    def surface_area(self):
        top = Cylinder.pi * self.radius**2

        area = 2 * top + 2 * Cylinder.pi * self.height * self.radius

        print(area)


c = Cylinder(2, 3)

c.volume()
c.surface_area()
