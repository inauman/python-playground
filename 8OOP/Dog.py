from Animal import Animal

class Dog(Animal):
    # Class object attributes
    # Same for any class
    species = 'mammal'

    def bark(self):
        print(f'WOOF! I am {self.name}')

    def color(self):
        print("I am black")
        
my_dog = Dog('Lab')
my_dog.bark()
my_dog.color()