class Account():
    
    def __init__(self, owner, balance):
        self.owner = owner
        self.balance = balance
        
    def deposit(self, money_in):
        self.balance += money_in

    def withdraw(self, money_out):
        if self.balance < money_out:
            print("You can not overdraw")
        else:
            self.balance -= money_out
            print(f'You are taking out {money_out} and now your new balance is {self.balance}')

    def __str__(self):
        return f"{self.owner}, your balance is {self.balance}"

acct = Account("Mayesha", 1000)
print(acct)
acct.deposit(3000)
print(acct)
acct.withdraw(200)
print(acct)

