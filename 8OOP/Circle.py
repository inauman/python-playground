class Circle():
    # Class attribute
    pi = 3.14

    def __init__(self, radius=1):
        self.radius = radius

    def get_circumference(self):
        return 2*Circle.pi*self.radius

my_circle = Circle(30)

print(f'Circumference of the circle is {my_circle.get_circumference()}')