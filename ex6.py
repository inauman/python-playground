#!/usr/bin/env python3

word = input('Enter the word here: ')

def capi(word):
  part1 = word[:3]
  part2 = word[3:]

  return part1.capitalize() + part2.capitalize()

print(capi(word))
