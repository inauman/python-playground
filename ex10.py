#!/usr/bin/env python3

def str_multiplier(st):
    wordlst = ["".join(ch*3) for ch in st]
    return "".join(wordlst) 
print(str_multiplier("hello"))
print(str_multiplier("sam"))