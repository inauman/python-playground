from BasePerson1 import Person1

class Student(Person1):
    def __init__(self, _fname, _lname, _year):
        super().__init__(_fname, _lname)
        self.graduationyear = _year

    def welcome(self):
        print("Welcome", self.firstname, self.lastname, "to class of", self.graduationyear)


x = Student("Mayesha", "Nauman", "2000")
x.printme()
x.welcome()