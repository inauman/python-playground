x = "awesome"

def myfunc():
    global x #a global variable can be declared inside a fun using global keyword
    x = "fantastic"
    print("Python is "+x)

myfunc()

print("Python is "+x)