#!/usr/bin/env python3

num = int(input("Enter the number: "))

def check_the_num(num):
    return (abs(100-num) <= 10) or (abs(200-num) <= 10)

print(check_the_num(num))
