from collections import namedtuple

Car = namedtuple('Car', 'color mileage')

my_car = Car('red', 3800.2)

print(my_car.color)

my_car.color = 'blue'

#namesdtuples are immutable so the following code will give an error
print(my_car.color)