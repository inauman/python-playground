#given list items
mylist = ["a", "b", "a", "c", "c"]

#as dict can't have dups, convert list --> dict
mydict = dict.fromkeys(mylist)

#convert dict back to list
mylist = list(mydict)

print(mylist)


#reverse a string
txt = "Hello World"[::-1]
print(txt)