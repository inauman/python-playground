'''
#define the function with 'def' keyword
def my_function(name):
    #print("hello from a function " + name)
    print("hello {} from a function".format(name))

#call the function
my_function("Mohammad Nauman")
'''

'''
#arbitrary arguments i.e. unknown number of arguments

def my_function(*kids):
    print("The youngest child is: " + kids[1])

my_function("Ayesha", "Maaz", "Mayesha")
'''
'''
#named arguments
def my_function(child3, child1, child2):
    print("The youngest child is: " + child3)

my_function(child1 = "Ayesa", child2 = "Maaz", child3 = "Mayesha")
'''
'''
def my_function(**kids):
    print("her first name is: "+ kids["fname"])

my_function(fname = "Ayesha", lname = "Nauman")
'''
'''
def my_function(country = "USA"):
    print("I am from: "+ country)

my_function("India")
my_function()
'''
'''
def my_function(colors):
    for x in colors:
        print(x)

my_function(["red", "orange", "pinl", "blue"])
'''

def square_me(x):
    return x*x

print(square_me(5))
